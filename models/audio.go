package models

import "github.com/jinzhu/gorm"

// Audio contains audio info
type Audio struct {
	gorm.Model
	Title   string
	Artist  string
	CoverID string
}
