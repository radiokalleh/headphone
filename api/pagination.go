package api

import (
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/radiokalleh/headphone/db"
	"gitlab.com/radiokalleh/headphone/models"
	pb "gitlab.com/radiokalleh/protos/proto_go/headphone"
	"golang.org/x/net/context"
)

func (hs *headphoneService) GetMainPageList(ctx context.Context,
	request *pb.GetMainPageListRequest) (*pb.GetMainPageListResponse, error) {

	logrus.Debug("[grpc] GetMainPageList")

	// Query audios from database
	audios := []models.Audio{}
	db.GetInstance().Find(&audios)
	logrus.Debugf("all audios: %v", audios)

	// Extract audio details to response
	audiosDetails := []*pb.AudioDetails{}
	for _, audio := range audios {
		// TODO: Generate coverURL
		detail := &pb.AudioDetails{
			Id:         strconv.FormatUint(uint64(audio.ID), 10),
			Title:      audio.Title,
			ArtistName: audio.Artist,
			CoverURL:   audio.CoverID,
		}
		audiosDetails = append(audiosDetails, detail)
	}

	response := &pb.GetMainPageListResponse{
		AudiosDetails: audiosDetails,
	}
	return response, nil
}
