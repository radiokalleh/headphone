package api

import (
	"io"
	"io/ioutil"
	"path"
	"strconv"

	"gitlab.com/radiokalleh/headphone/db"
	"gitlab.com/radiokalleh/headphone/models"

	"github.com/sirupsen/logrus"
	pb "gitlab.com/radiokalleh/protos/proto_go/headphone"
)

func saveAudioToFile(filename string, data []byte) error {
	// TODO: Get storage dir from config
	return ioutil.WriteFile(path.Join("./storage", filename), data, 0644)
}

func (hs *headphoneService) AddAudio(stream pb.HeadphoneService_AddAudioServer) error {
	logrus.Debug("[grpc] AddAudio")

	audioBytes := []byte{}
	var audioDetails *pb.AudioDetails

	for {
		request, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			logrus.WithError(err).Error("Can't read AddAudio's stream")
			return err
		}

		switch request.GetRequestSelector().(type) {
		case *pb.AddAudioRequest_Bulk:
			audioBytes = append(audioBytes, request.GetBulk().Data...)
		case *pb.AddAudioRequest_Details:
			audioDetails = request.GetDetails()
		}
	}
	logrus.WithField("len", len(audioBytes)).Debug("AudioBulks fetched completely")

	audioModel := &models.Audio{
		Title:   audioDetails.GetTitle(),
		Artist:  audioDetails.GetArtistName(),
		CoverID: audioDetails.GetCoverURL(), // TODO: URL vs ID
	}
	db.GetInstance().Create(audioModel)

	filename := strconv.FormatUint(uint64(audioModel.ID), 10)
	saveAudioToFile(filename, audioBytes)

	return nil
}
