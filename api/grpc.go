package api

import (
	"fmt"
	"net"

	"github.com/sirupsen/logrus"
	"gitlab.com/radiokalleh/headphone/configuration"
	pb "gitlab.com/radiokalleh/protos/proto_go/headphone"
	"google.golang.org/grpc"
)

type headphoneService struct {
}

// StartGRPCServer creates a new GRPC server and starts it.
// It's a blocking function.
func StartGRPCServer() error {
	host := configuration.GetInstance().GetString("servers.api.host")
	port := configuration.GetInstance().GetInt("servers.api.port")

	tcpListener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		logrus.WithError(err).Fatal("failed to listen")
	}

	logrus.Info("GRPC server is going to start")
	grpcServer := grpc.NewServer()
	pb.RegisterHeadphoneServiceServer(grpcServer, &headphoneService{})
	if err := grpcServer.Serve(tcpListener); err != nil {
		logrus.WithError(err).Fatal("failed to serve")
	}
	return err
}
