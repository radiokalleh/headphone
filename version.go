package headphone

import (
	"os"
	"time"
)

var (
	// Version indicates binary's version
	Version string
	// Commit contains latest commit hash
	Commit string
	// BuildTime indicates when binary had been compiled
	BuildTime string
	// RootDirectory is root of project
	RootDirectory string
	// Title is name of project
	Title string
	// StartTime is time of execution
	StartTime time.Time
)

func init() {
	if Version == "" {
		Version = "unknown"
	}
	if Commit == "" {
		Commit = "unknown"
	}
	if BuildTime == "" {
		BuildTime = "unknown"
	}
	if RootDirectory == "" {
		RootDirectory = os.Getenv("HOME") + "/headphone"
	}
	if Title == "" {
		Title = "headphone"
	}
	StartTime = time.Now()
}
