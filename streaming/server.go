package streaming

import (
	"fmt"
	"net/http"

	"github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"gitlab.com/radiokalleh/headphone/providers"
)

// Server specifically is a streaming server. It serves audio contents over
// Websocket. Also it has `healthz` and `test` pages to make itself testable.
type Server struct {
	AudioProvider providers.Provider
	Host          string
	Port          int
}

// ListenAndServe regiters all needed handlers and MUXs and starts listening
// on specified host and port.
func (s *Server) ListenAndServe() error {
	router := mux.NewRouter()
	router.HandleFunc("/", s.socketHandler)
	router.HandleFunc("/test", s.testHandler)
	router.HandleFunc("/healthz", s.healthzHandler)
	return http.ListenAndServe(fmt.Sprintf("%s:%d", s.Host, s.Port), router)
}

// ListenAndServe creates a Server and makes it start listening
func ListenAndServe(host string, port int, provider providers.Provider) error {
	server := &Server{
		Host:          host,
		Port:          port,
		AudioProvider: provider,
	}
	logrus.Info("Streamer server is going to start")
	return server.ListenAndServe()
}
