package streaming

import (
	"net/http"

	"github.com/golang/protobuf/proto"

	"github.com/sirupsen/logrus"
	"gitlab.com/radiokalleh/headphone/providers"
	pb "gitlab.com/radiokalleh/protos/proto_go/headphone"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{}

func (s *Server) healthzHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Fine!"))
}

func (s *Server) testHandler(w http.ResponseWriter, r *http.Request) {
	testTemplate.Execute(w, "ws://"+r.Host+"/")
}

func (s *Server) socketHandler(w http.ResponseWriter, r *http.Request) {
	// Upgrade connection to websocket
	logrus.Debug("new request to Websocket url")
	socket, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logrus.WithError(err).Error("can't upgrade connection")
		return
	}
	defer socket.Close()
	logrus.Debug("connection upgraded to Websocket")

	// Read message
	logrus.Debug("reading message on websocket")
	messageType, message, err := socket.ReadMessage()
	if err != nil || messageType != websocket.BinaryMessage {
		logrus.WithError(err).Error("can't parse play request")
		return
	}
	playRequest := &pb.PlayRequest{}
	if proto.Unmarshal(message, playRequest) != nil {
		logrus.WithError(err).Error("can't unmarshaling play request")
	}
	logrus.WithField("request", *playRequest).Debug("message read on websocket")

	// Start sending streams over websocket
	audioChan := make(chan providers.AudioBulk, 4096)
	s.AudioProvider.Subscribe(playRequest.GetMediaID(), 0, audioChan)
	for {
		protoAudioBulk := &pb.AudioBulk{}
		protoAudioBulk.Data = <-audioChan
		bulk, err := proto.Marshal(protoAudioBulk)

		err = socket.WriteMessage(websocket.BinaryMessage, bulk)
		if err != nil {
			logrus.WithError(err).Error("can't write message")
			break
		}
	}
}
