package streaming

import (
	"gitlab.com/radiokalleh/headphone/db"
	"gitlab.com/radiokalleh/headphone/models"
)

func getAudio(audioID string) *models.Audio {
	var audio *models.Audio
	db.GetInstance().First(audio, audioID)
	return audio
}
