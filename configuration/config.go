package configuration

import (
	"sync"

	"github.com/fsnotify/fsnotify"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	configFilePath  = "config.yaml"
	headphoneConfig *viper.Viper

	once sync.Once
)

// GetInstance returns an instance of viper config
func GetInstance() *viper.Viper {
	once.Do(func() {
		loadConfig()
	})
	return headphoneConfig
}

func loadConfig() {
	config := viper.New()

	// Setting defaults for this application
	config.SetDefault("debug", true)

	if configFilePath != "" {
		config.SetConfigFile(configFilePath)

		config.OnConfigChange(OnConfigChanged)
		config.WatchConfig()

		err := config.ReadInConfig()
		if err != nil {
			logrus.Errorf("Can't read config file, %v", err)
			headphoneConfig = config
			return
		}
		logrus.Infof("Configuration file is loaded from %s", configFilePath)
	}
	SetDebugLogLevel(config.GetBool("debug"))

	logrus.Debugf("Loaded config: %v", config.AllSettings())
	headphoneConfig = config
}

// SetFilePath sets path of config file
func SetFilePath(filePath string) {
	configFilePath = filePath
	headphoneConfig = nil
}

// SetDebugLogLevel sets log level to debug mode
func SetDebugLogLevel(isDebug bool) {
	if isDebug {
		logrus.SetLevel(logrus.DebugLevel)
		logrus.Debug("Log level is set to Debug")
	} else {
		logrus.SetLevel(logrus.InfoLevel)
	}
}

// OnConfigChanged excuates when config changes
func OnConfigChanged(_ fsnotify.Event) {
	loadConfig()
	logrus.Info("Configuration is reloaded")
}
