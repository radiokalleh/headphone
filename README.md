# Headphone

[![pipeline status](https://gitlab.com/radiokalleh/headphone/badges/master/pipeline.svg)](https://gitlab.com/radiokalleh/headphone/commits/master) [![coverage report](https://gitlab.com/radiokalleh/headphone/badges/master/coverage.svg)](https://gitlab.com/radiokalleh/headphone/commits/master)

Headphone is a microservice (part of Noise project) which serves audio data to listeners.

## Build and Run
This service is **not production-ready** but you may run it using following commands:
``` bash
$ make
$ ./headphoned serve
```

## Maintainer
Arya Hadi (arya.hadi97@gmail.com)
