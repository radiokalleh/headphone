package providers

import (
	"io/ioutil"
	"math"

	"gitlab.com/radiokalleh/headphone/configuration"
)

// Dummy is a provider which publishs a sample wav file
type Dummy struct {
	BulkSize int
}

// Subscribe tries to fill the channel with sample wav file's bytes
// It's not blocking.
func (d *Dummy) Subscribe(mediaID string, offset Offset, channel chan AudioBulk) error {
	sampleFile, err := ioutil.ReadFile(configuration.GetInstance().GetString("sample-audio"))
	if err != nil {
		return err
	}

	go func(bulkSize int) {
		idx := int(offset)
		for idx < len(sampleFile) {
			length := int(math.Min(float64(bulkSize), float64(len(sampleFile)-idx)))
			channel <- sampleFile[idx : idx+length]
			idx += bulkSize
		}
	}(d.BulkSize)
	return nil
}
