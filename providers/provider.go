package providers

// AudioBulk is a part of audio file's data
type AudioBulk []byte

// Offset is not standard yet. It may be index of an audio bulk or may be time.
type Offset uint

// Provider is an interface for all sources of audio.
// Audio sources are some objects than can provide contents.
type Provider interface {
	Subscribe(mediaID string, offset Offset, channel chan AudioBulk) error
}
