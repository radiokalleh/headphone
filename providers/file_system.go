package providers

import (
	"bufio"
	"io"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"

	"gitlab.com/radiokalleh/headphone/configuration"
)

// FileSystem is a provider which delivers data from local filesystem
type FileSystem struct {
	BulkSize int
}

// Subscribe tries to fill the channel with bytes of related media
func (fs *FileSystem) Subscribe(mediaID string, offset Offset, channel chan AudioBulk) error {
	storageDir := configuration.GetInstance().GetString("storage.dir")
	mediaFilePath := filepath.Join(storageDir, mediaID)

	mediaFile, err := os.Open(mediaFilePath)
	if err != nil {
		return err
	}

	go func(mediaFile *os.File, bulkSize int) {
		reader := bufio.NewReader(mediaFile)
		for {
			// FIXME: This shouldn't be like this! I mean don't create a new buffer each time
			buffer := make([]byte, bulkSize)
			_, err := reader.Read(buffer)
			if err != nil {
				if err != io.EOF {
					logrus.WithError(err).WithField("mediaID", mediaID).
						Error("can't read media file")
				}
				break
			}

			channel <- buffer
		}
	}(mediaFile, fs.BulkSize)

	return nil
}
