package db

import (
	"sync"

	"github.com/jinzhu/gorm"
	// sqlite dialect
	_ "github.com/jinzhu/gorm/dialects/sqlite"

	"github.com/sirupsen/logrus"
	"gitlab.com/radiokalleh/headphone/configuration"
	"gitlab.com/radiokalleh/headphone/models"
)

var (
	db   *gorm.DB
	once sync.Once
)

func getSqliteDB(path string) (*gorm.DB, error) {
	return gorm.Open("sqlite3", path)
}

// GetInstance returns a gorm DB singleton
func GetInstance() *gorm.DB {
	once.Do(func() {
		err := initDB()
		if err != nil {
			logrus.Errorf("can't init database")
		}
	})
	return db
}

// TODO: call this func when config changes
func initDB() error {
	var err error
	filePath := configuration.GetInstance().GetString("db.path")
	logrus.Debug("initializing Sqlite3 connection")
	db, err = getSqliteDB(filePath)

	autoMigrate()

	return err
}

func autoMigrate() {
	db.AutoMigrate(&models.Audio{})
}

// Close singleton DB instance
func Close() error {
	return db.Close()
}
