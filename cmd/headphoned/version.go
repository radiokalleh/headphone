package main

import (
	"gitlab.com/radiokalleh/headphone"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "headphone <subcommand>",
	Short: "Voice Recording Service",
	Run: func(cmd *cobra.Command, args []string) {

	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}

func logVersion() {
	logrus.Info("version   > ", headphone.Version)
	logrus.Info("buildtime > ", headphone.BuildTime)
	logrus.Info("commit    > ", headphone.Commit)
}
