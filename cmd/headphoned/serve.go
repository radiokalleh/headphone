package main

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/radiokalleh/headphone/api"
	"gitlab.com/radiokalleh/headphone/configuration"
	"gitlab.com/radiokalleh/headphone/providers"
	"gitlab.com/radiokalleh/headphone/streaming"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start server",
	Run:   serve,
}

func init() {
	rootCmd.AddCommand(serveCmd)
}

func serve(cmd *cobra.Command, args []string) {
	logVersion()

	go startAPIServer()

	startStreamer()
}

func startAPIServer() {
	if err := api.StartGRPCServer(); err != nil {
		logrus.WithError(err).Fatal("can't start GRPC")
	}
}

func startStreamer() {
	host := configuration.GetInstance().GetString("servers.streamer.host")
	port := configuration.GetInstance().GetInt("servers.streamer.port")

	bulkSize := configuration.GetInstance().GetInt("servers.streamer.bulk-size")
	provider := &providers.FileSystem{BulkSize: bulkSize}

	logrus.Fatal(streaming.ListenAndServe(host, port, provider))
}
