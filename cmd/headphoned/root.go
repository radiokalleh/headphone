package main

import (
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "headphone <subcommand>",
	Short: "Audio Streaming Service",
	Run:   nil,
}

func init() {
	cobra.OnInitialize()
}
