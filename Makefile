ifdef CI_JOB_TOKEN
GITLAB_URL := "https://gitlab-ci-token:$(shell echo $(CI_JOB_TOKEN))@gitlab.com/"
endif
ifndef CI_JOB_TOKEN
GITLAB_URL := "git@gitlab.com:"
endif

PROJECT_NAME := "headphone"
PKG := "gitlab.com/radiokalleh/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
COMMIT := $(shell git rev-parse HEAD)
VERSION ?= $(shell git describe --tags ${COMMIT} 2> /dev/null || echo "$(COMMIT)")
BUILD_TIME := $(shell LANG=en_US date +"%F_%T_%z")
ROOT_DIRECTORY := $(shell pwd)
LD_FLAGS := -X $(ROOT).Version=$(VERSION) -X $(ROOT).Commit=$(COMMIT) -X $(ROOT).BuildTime=$(BUILD_TIME) -X $(ROOT).RootDirectory=$(ROOT_DIRECTORY) -X $(ROOT).Title=$(PROJECT_NAME)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

dep: Gopkg.lock Gopkg.toml ## Get the dependencies
	git config --global url.${GITLAB_URL}.insteadOf "https://gitlab.com/"
	dep ensure

build: ## Build the binary file
	@go build -o="$(PROJECT_NAME)d" -ldflags="$(LD_FLAGS)" -i -v $(PKG)/cmd/headphoned

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)d

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
